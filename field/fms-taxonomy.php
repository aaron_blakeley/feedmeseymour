<?php


//hook into the init action and call create_topics_nonhierarchical_taxonomy when it fires

add_action( 'init', 'fms_tax', 0 );

function fms_tax() {

// Labels part for the GUI

  $labels = array(
    'name' => 'FMS Tags',
    'singular_name' => 'FMS tag',
    'search_items' =>  'Search FMS Tags',
    'popular_items' => 'Popular FMS Tags',
    'all_items' => 'All FMS Tags',
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => 'Edit FMS Tag',
    'update_item' => 'Update FMS Tag',
    'add_new_item' => 'Add New FMS Tag',
    'new_item_name' => 'New FMS Tag Name',
    'separate_items_with_commas' => 'Separate fms tags with commas',
    'add_or_remove_items' => 'Add or remove fms tags',
    'choose_from_most_used' => 'Choose from the most used fms tags',
    'menu_name' => 'FMS Tags',
  );

  // Now register the non-hierarchical taxonomy like tag

  register_taxonomy('fms_tags','fms_post',array(
    'hierarchical' => false,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'fms-topic' ),
  ));
}
