<?php
function fms_post_type() {

	/**
	 * Post Type: empployee annoucement
	 */

	$labels = array(
		"name" => __( 'FMS Posts', '' ),
		"singular_name" => __( 'FMS Post', '' ),
	);

	$args = array(
		"label" => __( 'FMS Post', '' ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
    "exclude_from_search" => false,
    "publicly_queryable" => true,
    "show_ui" => true,
    "show_in_rest" => true,
    "show_in_menu" => true,
    "show_in_nav_menus" => false,
    "show_in_admin_bar" => false,
    "menu_position" => 5,
    // "capabilities" => array(
    //
		// 	'edit_post'          => 'edit_employee_announcement',
		// 	'read_post'          => 'read_employee_announcement',
		// 	'delete_post'        => 'delete_employee_announcement',
		// 	'edit_posts'         => 'edit_employee_announcements',
		// 	'edit_others_posts'  => 'edit_others_employee_announcements',
		// 	'publish_posts'      => 'publish_employee_announcements',
		// 	'read_private_posts' => 'read_private_employee_announcements',
		// 	'create_posts'       => 'edit_employee_announcements',
    //
		// ),
    "map_meta_cap" => true,
    "hierarchical" => false,
    "rest_base" => "fms_v1",
    "has_archive" => true,
    "supports" => array('comments', 'author'),
    "delete_with_user" => false,
		"rewrite" => array( "slug" => "fms", "with_front" => true, ),
		"query_var" => true
	);

	register_post_type( "fms_post", $args );
}

add_action( 'init', 'fms_post_type', 0 );

//acf here

if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
	'key' => 'group_5d1c0f86d4130',
	'title' => 'FMS Posts',
	'fields' => array(
		array(
			'key' => 'field_5d1c0fa7664c8',
			'label' => 'FMS Type',
			'name' => 'fms_type',
			'type' => 'select',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'choices' => array(
				'Text Only' => 'Text Only',
				'Text and Image' => 'Text and Image',
				'Text and Video Embed' => 'Text and Video Embed',
				'Text and Video Upload' => 'Text and Video Upload',
				'Text and Link' => 'Text and Link',
				'Text and Audio' => 'Text and Audio',
				'Text and File' => 'Text and File',
			),
			'default_value' => array(
			),
			'allow_null' => 0,
			'multiple' => 0,
			'ui' => 0,
			'return_format' => 'value',
			'ajax' => 0,
			'placeholder' => '',
		),
		array(
			'key' => 'field_5d1c1044664ca',
			'label' => 'Single Image',
			'name' => 'single_image',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and Image',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'preview_size' => 'large',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5d1c10ed08204',
			'label' => 'Youtube Embed',
			'name' => 'youtube_embed',
			'type' => 'oembed',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and Video Embed',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'width' => '',
			'height' => '',
		),
		array(
			'key' => 'field_5d1c124a08205',
			'label' => 'Video Upload',
			'name' => 'video_upload',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and Video Upload',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'all',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => 'mp4',
		),
		array(
			'key' => 'field_5d2d3410091a9',
			'label' => 'Link Title',
			'name' => 'link_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and Link',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
		),
		array(
			'key' => 'field_5d1c12d408206',
			'label' => 'A Link',
			'name' => 'a_link',
			'type' => 'url',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and Link',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
		),
		array(
			'key' => 'field_5d1c12fe08207',
			'label' => 'Audio',
			'name' => 'audio',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and Audio',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'all',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => 'wav, mp3, m4a',
		),
		array(
			'key' => 'field_5d1c138508208',
			'label' => 'File',
			'name' => 'file',
			'type' => 'file',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => array(
				array(
					array(
						'field' => 'field_5d1c0fa7664c8',
						'operator' => '==',
						'value' => 'Text and File',
					),
				),
			),
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'url',
			'library' => 'all',
			'min_size' => '',
			'max_size' => '',
			'mime_types' => '',
		),
		array(
			'key' => 'field_5d1c1006664c9',
			'label' => 'Your Post Text',
			'name' => 'your_post_text',
			'type' => 'wysiwyg',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array(
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'tabs' => 'visual',
			'toolbar' => 'basic',
			'media_upload' => 0,
			'delay' => 0,
		),
	),
	'location' => array(
		array(
			array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'fms_post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
));

endif;
