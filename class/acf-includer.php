<?php

/**
* Includes ACF
*/
class ACFIncluder
{

  static function initialize()
	{
		// 1. customize ACF path

		add_filter('lib/acf/settings/path', 'my_acf_settings_path');

		function my_acf_settings_path( $path ) {

		    // update path
		    $path = plugin_dir_path(__FILE__) . '../lib/acf/';

		    // return
		    return $path;

		}


		// 2. customize ACF dir
		add_filter('lib/acf/settings/dir', 'my_acf_settings_dir');

		function my_acf_settings_dir( $dir ) {

		    // update path
		    $dir = plugin_dir_path(__FILE__) . '../lib/acf/';

		    // return
		    return $dir;

		}


		// 3. Hide ACF field group menu item
		add_filter('lib/acf/settings/show_admin', '__return_false');


		// 4. Include ACF
		include_once( plugin_dir_path(__FILE__) . '../lib/acf/acf.php' );

		return true;

	}
}
ACFIncluder::initialize();
