<?php
/*
Plugin Name: Feed Me Seymour
Plugin URI: https://aaronblakeley.com
Description: Feed Me Seymour is the foundation for the new blogging movement.
Version: 0.0.5
Author: hitcode.com
Author URI: https://aaronblakeley.com
*/
define("FMS_DIR", dirname(__FILE__));
include FMS_DIR."/class/acf-includer.php";
include FMS_DIR."/field/fms-posts.php";
include FMS_DIR."/field/fms-taxonomy.php";
include FMS_DIR."/fms-list-short.php";

function fms_enqueue_scripts(){
  wp_enqueue_style('fms-styles', plugin_dir_url( __FILE__ )."fms-styles.css", array(), "1.0");
}

add_action("wp_enqueue_scripts", 'fms_enqueue_scripts');

function limit_text($text, $limit) {
  if (str_word_count($text, 0) > $limit) {
    $words = str_word_count($text, 2);
    $pos = array_keys($words);
    $text = substr($text, 0, $pos[$limit]) . '...';
  }
  return strip_tags($text);
}


add_action('save_post', 'save_FMS_post');
function save_FMS_post($data){
  global $post;
  global $wpdb;
    $post_type = get_post_type($post->ID);
    if($post_type == "fms_post"){
      $name = $post->ID;
      $post_text = get_field('your_post_text', $post->ID);
      if(str_word_count($post_text) > 0){
        $title = limit_text($post_text, 4);
      }else{
        $title = "Feed Me Seymour Post - [".get_field('fms_type' ,$post->ID)."]";
      }
      $wpdb->update($wpdb->prefix."posts", array('post_title' => $title,'post_name' => $name),array('ID' => $post->ID));
    }
}

add_filter('the_content', 'fms_the_content');
add_filter('the_excerpt','fms_the_content');

function fms_the_content($content){
  global $post;
  $post_type = get_post_type($post->ID);
  if($post_type == "fms_post"){
    $fms_type = get_field('fms_type', $post->ID);

    switch ($fms_type) {
      case 'Text Only':
        $return = fms_text_only($post->ID);
      break;

      case 'Text and File':
        $return = fms_text_file($post->ID);
      break;

      case 'Text and Image':
        $return = fms_text_image($post->ID);
      break;

      case 'Text and Video Embed':
        $return = fms_text_video_embed($post->ID);
      break;

      case 'Text and Video Upload':
        $return = fms_text_video_upload($post->ID);
      break;

      case 'Text and Link':
        $return = fms_text_link($post->ID);
      break;

      case 'Text and Audio':
        $return = fms_text_audio($post->ID);
      break;

      default:
        $return = fms_default($post->ID);
      break;
    }
    return $return;
  }else{
    return $content;
  }
}

//add_filter('the_title', 'fms_the_title');

// function fms_the_title($title){
//   global $post;
//
//     $post_type = get_post_type($post->ID);
//     if($post_type == "fms_post" && ((is_singular()||is_archive())&&!is_admin())){
//       return $title;
//     }else{
//       return $title;
//     }
//
// }


function fms_text_only($id){
  $return = "<div>".get_field('your_post_text',$id)."</div>";

  return fms_wrapper($return, $id);
}
function fms_text_file($id){
  $file = get_field('file',$id);
  $file_name = explode('/',$file);
  $index = (count($file_name)-1);
  $file_name = $file_name[$index];
  $content = get_field('your_post_text',$id);
  $return = "<div>";
  $return .= "<p><a class='fms-button button' href='".$file."'>Download $file_name</a></p>";
  $return .= "$content";
  // $return .= "<a class='fms-link-just-the-post-mam' href='".get_the_permalink($id)."'>View Just This Post</a>";
  $return .= "</div>";
  return fms_wrapper($return, $id);
}
function fms_text_image($id){
  $image = get_field('single_image',$id);
  $content = get_field('your_post_text',$id);
  $return = "<div>";
  $return .= "<div class='FMS-single-image'><img src='$image' /></div>";
  $return .= "<div>".$content."</div>";
  // $return .= "<a class='fms-link-just-the-post-mam' href='".get_the_permalink($id)."'>View Just This Post</a>";
  $return .= "</div>";
  return fms_wrapper($return, $id);
}
function fms_text_video_embed($id){
  $content = get_field('your_post_text',$id);
  $embed = get_field('youtube_embed',$id);
  $return = "<div>".$embed."</div>";
  $return .= "<div>".$content."</div>";
  // $return .= "<a class='fms-link-just-the-post-mam' href='".get_the_permalink($id)."'>View Just This Post</a>";
  return fms_wrapper($return, $id);
}
function fms_text_video_upload($id){
  $content = get_field('your_post_text',$id);
  $video = get_field('video_upload',$id);
  $return ="<div>";
  $return .= "<video src='$video' controls style='width:100%'></video>";
  $return .= "</div>";
  $return .= "<div>".$content."</div>";
  // $return .= "<a class='fms-link-just-the-post-mam' href='".get_the_permalink($id)."'>View Just This Post</a>";
  return fms_wrapper($return, $id);
}
function fms_text_link($id){
  $link = get_field('a_link', $id);
  $link_title = get_field('link_title', $id);
  $content = get_field('your_post_text',$id);
  $return = "<div><a class='fms-button button' href='$link'>".$link_title."</a></div>";
  $return .= "<div>".$content."</div>";
  // $return .= "<a class='fms-link-just-the-post-mam' href='".get_the_permalink($id)."'>View Just This Post</a>";
  return fms_wrapper($return, $id);
}
function fms_text_audio($id){
  $audio_file = get_field('audio',$id);
  $audio_type = explode(".", $audio_file);
  $index = count($audio_type)-1;
  $audio = "<audio controls  src='$audio_file'>";
  if($audio_type[$index] == "wav"){
    $audio .=  "<source src='$audio_file' type='audio/wav'>";
  }
  if($audio_type[$index] == "mp3"){
    $audio .= "<source  src='$audio_file' type='audio/mpeg'>";
  }
  $audio .= "Your browser does not support the audio element.";
  $audio .= "</audio>";
  $content = get_field('your_post_text',$id);
  $return = "<div>".$audio."</div>";
  $return .= "<div>".$content."</div>";
  // $return .= "<a class='fms-link-just-the-post-mam' href='".get_the_permalink($id)."'>View Just This Post</a>";
  return fms_wrapper($return, $id);
}
function fms_default($id){
  $content = fms_text_only($id);
  $return .= "<div>".$content."</div>";
  return "";
}

function fms_tag_retrival($id){
  $terms = wp_get_object_terms( $id, 'fms_tags');
  $return = "<div class='fms-tags'>";
  foreach($terms as $term){
    $return .= "<a href='".get_term_link($term)."'>".$term->name."</a> ";
  }
  $return .= "</div>";
  return $return;
}

function fms_wrapper($content, $id){
    $return = "<div class='fms-post-wrap'>".$content;
    $return .= fms_tag_retrival($id);
    $return .= "</div>";
    return $return;
}
