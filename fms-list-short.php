<?php
/*
	this is not in use yet.
*/

function fms_list_display($atts){

	$atts = shortcode_atts(
		array(
			'page-slug' => 'sample-page'
		), $atts);

	$thePage = get_query_var('paged');

	$args = array(
		'post_type'      => 'fms_post',
		'posts_per_page' => 2,
		'paged'          => $thePage
	);
	$message = false;

	$fmsQuery = new WP_Query($args);
	ob_start();
	fms_page($fmsQuery, $atts['page-slug']);
	if($fmsQuery->have_posts()){
		while($fmsQuery->have_posts()){
			$fmsQuery->the_post();
			echo"<div>".the_content()."</div>";
		}
	}
	fms_page($fmsQuery, $atts['page-slug']);
	$return = ob_get_contents();
	ob_end_clean();
	return $return;
}
add_shortcode('fms-list','fms_list_display');




function fms_page($fmsQuery, $page_slug){
	$maxPages = $fmsQuery->max_num_pages;
	$theRange = array();
	$theRange['high'] = ($thePage+4 > $maxPages ? $maxPages : $thePage+4);
	$theRange['low'] = ($thePage-4 < 0 ? 0 : $thePage-4);

	$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri_segments = explode('/', $uri_path);
	$uri_back = $uri_segments[3]-1;
	if ($uri_segments[3]==""){
		$uri_next = '2';
	}else{
		$uri_next = $uri_segments[3]+1;
	}

	if($maxPages>1){ // This is the paging
		echo"<ul class='paging-fms'>";

		if ($uri_segments[3] > 0 && $uri_segments[3]!=""){?>
			<li><a href="/<?php echo $page_slug ?>/page/<?php echo $uri_back ?>/">&larr;</a></li>
			<?php
		}
				for($i=1; $i<=$maxPages; $i++){
					//if((($i!=1)&&($i!=$maxPages))&&(($i < $theRange['high']) && ($i > $theRange['low']))){ // Test for the first and last page and the range of at least 8 pages.
						?>

						<li class="<?php if ($uri_segments[3] == $i || ($uri_segments[3] == "" && $i == 1) ){echo 'active';} ?>"><a href="/<?php echo $page_slug ?>/page/<?php echo $i ?>/"><?php echo $i ?></a></li>
						<?php
					//}
				}
			?>
		<?php //echo $page_slug ?><?php //echo $fmsQuery->max_num_pages ?>
		<?php
		if ($uri_segments[3] < $fmsQuery->max_num_pages){?>
			<li><a href="/<?php echo $page_slug ?>/page/<?php echo $uri_next ?>/">&rarr;</a></li>
		<?php }
		echo"</ul>";
	}
}
